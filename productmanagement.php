<?php
require_once "bootstrap.php";
require_once "components/Layout.php";
require_once "utils/ForbiddenTools.php";

only("Seller");

Layout(
    "Home",
    "La Home page del sito di e-commerce dei tuoi sogni",
    "template/ManageProducts.php",
    "template/SideAd.php"
);
