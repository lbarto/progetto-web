# Dream shopper

Portale per la vendita e l'acquisto dei sogni online

## Descrizione delle funzionalità

Dream shopper permette di iscriversi come cliente o venditore. Il venditore può mettere in vendita sogni, che appariranno nel catalogo. Il cliente, anche prima di aver fatto login, può esplorare il catalogo e aggiungere i sogni nel carrello. Al momento dell'acquisto sarà necessario iscriversi. All'acquisto, verrà inviata una notifica al venditore, segnalandogli che un suo prodotto è stato acquistato.

## Istruzioni

Per preparare il database, eseguire le query in dreamShopperDB_3.0.sql, poi popolare la tabella delle categorie di sogno eseguendo il file insertCategoria.sql

## Documenti

Il sito è stato realizzato in base ad un mockup, che si può trovare all'indirizzo https://www.figma.com/file/Br65vwwWzlDiB7BnctaGzS/Design-Study?node-id=0%3A1