function checkCity(){

  let badColor = "#ff6666";
  document.getElementById("confirmCity").style.color = badColor;

  let txt = $("#inputCity").val();

  if (txt.length<3){
    document.getElementById("confirmCity").innerHTML = "The city has to be at least 3 characters long.";
  }else if (txt.length>25){
    document.getElementById("confirmCity").innerHTML = "The city can not have more than 15 characters.";
  }else{
    let text = /^[A-Za-z]+$/.test(txt);
    if (!text){
      document.getElementById("confirmCity").innerHTML = "The city can contain only characters.";
    }else{
      document.getElementById("confirmCity").innerHTML = "";
      return true;
    }
  }
  return false;
}