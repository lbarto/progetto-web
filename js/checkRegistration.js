import{checkName} from 'name.js'
import { checkEmail } from './check_email';
import { checkCity } from './city';
import { controlPassword } from './control_password';
import { formhash } from './form';
import { checkSurname } from './surname';

export function checkRegistration(){
    checkName();
    checkSurname();
    controlPassword();
    checkEmail();
    checkTel();
    formhash();
    checkCity();
}
