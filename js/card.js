function checkCard(){

  var badColor = "#ff6666";
  document.getElementById("confirmCard").style.color = badColor;

  var txt = $("#inputCard").val();

  if (txt.length!=16) {
    document.getElementById("confirmCard").innerHTML = "Il numero di carta deve essere di 16 numeri.";
    return false;
  }else{
    document.getElementById("confirmCard").innerHTML = "";
  }

  return true;
}
