function controlPassword()
{

  /*TOFIX 
  Split the function in two cases, one for each focusout:
  case first password: control if it's blank and the length (for now)
  case repeated password: contro if it's blank, the length and if it matches with the first one
  */
    let pass = $('#idPassword').val();
    let repeatedPass = $("#idRepeatPassword").val();
    //Store the Confimation Message Object ...
    let messagePassword = document.getElementById('confirmPassword');
    let messageRepeatedPassword = document.getElementById('confirmRepeatedPassword');

    //Set the colors we will be using ...
    let goodColor = "#66cc66";
    let badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    messageRepeatedPassword.style.color = badColor;
    messagePassword.style.color = badColor;

    if(pass.length <= 8){
      messagePassword.innerHTML="La password deve essere lunga almeno 8 caratteri!";
    }else if((repeatedPass.length < pass.length) || !(pass==repeatedPass)){
      messageRepeatedPassword.innerHTML="Le password non coincidono!"
    }elseif (pass==repeatedPass){
      messagePassword.innerHTML ="";
      messageRepeatedPassword.style.color = goodColor;
      messageRepeatedPassword.innerHTML = "Le password coincidono!";
      return true;
    }

    return false;
}