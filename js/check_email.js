function checkEmail() {

  let badColor = "#ff6666";
  document.getElementById("confirmEmail").style.color = badColor;

  let txt = $("#inputEmail").val();

  let text=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(txt);
  if (!text){
    document.getElementById("confirmEmail").innerHTML = "Email has to be valid.";
    return false;
  }else{
    document.getElementById("confirmEmail").innerHTML = "";
    return true;
  }
}

