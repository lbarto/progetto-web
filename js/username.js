function checkName(){

  let badColor = "#ff6666";
  document.getElementById("confirmName").style.color = badColor;

  var txt = $("#inputName").val();

  if (txt.length<3){
    document.getElementById("confirmName").innerHTML = "Username deve essere lungo almeno 3 caratteri";
  }else if (txt.length>20){
    document.getElementById("confirmName").innerHTML = "Username deve avere massimo 20 caratteri.";
  }else{
    let text = /[\'^£$%&*()}{@#~?><>,|=_+¬-]/.test(txt);
    if (text){
      document.getElementById("confirmName").innerHTML = "Username puo' contenere solo caratteri e numeri.";
    }else{
      document.getElementById("confirmName").innerHTML = "";
      return true;
    }
  }
  return false;
}