<?php
require_once("./db/database.php");

function logout()
{
  if (isset($_POST['logout'])) {
    unset($_SESSION['username']);
    unset($_SESSION['type']);
    //$_SESSION = array();
    if(session_destroy()){
      $logout = true;
    }
  }
}

function signIn($type, $username, $birthday, $email, $pass, $city, $card, $photo, $db)
{

  $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
  // Crea una password usando la chiave appena creata.
  $password = hash('sha512', $pass . $salt);

  if ($type == "Client") {
    $insert_stmt = $db->getDB();
    $query = $insert_stmt->prepare(
      "INSERT INTO cliente (username, password, compleanno, email, citta, carta, foto, salt) 
       VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
    );
  } else {
    $insert_stmt = $db->getDB();
    $query = $insert_stmt->prepare(
      "INSERT INTO venditore (username, password, compleanno, email, citta, carta, foto, salt) 
       VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
    );
  }


  $query->bind_param("sssssiss", $username, $password, $birthday, $email, $city, $card, $photo, $salt);
  $final = $query->execute();
  if($final){
    $_SESSION['username']=$username;
    $_SESSION['type']=$type;
  }
  return $final;
}

/*
function check_notification($e_mail, $mysqli)
{
  if (isAdmin($e_mail, $mysqli) || ($e_mail == "admin")) {
    $e_mail = "admin";
  }
  if ($stmt = $mysqli->prepare("SELECT id FROM notifica WHERE visto = 0 AND destinatario = ? LIMIT 1")) {
    $stmt->bind_param('s', $e_mail);
    // Eseguo la query creata.
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id); // got the information
    $stmt->fetch();
    if (is_null($id)) {
      return false;
    }
  }
  $visto = 1;
  $query_sql = "UPDATE notifica SET notifica.visto ='" . $visto . "'
                 WHERE notifica.id='" . $id . "'";
  $result = $mysqli->query($query_sql);
  return true;
}

function invia_notifica($mtt, $dst, $messaggio, $mysqli)
{
  if (isAdmin($mtt, $mysqli) || ($mtt == "admin")) {
    $mtt = "admin";
  } else if (isAdmin($dst, $mysqli) || ($mtt == "admin")) {
    $dst = "admin";
  }
  $query_sql = "INSERT INTO notifica (destinatario, messaggio, visto, mittente)
                  VALUES ('$dst', '$messaggio', 0, ' $mtt')";
  $result = $mysqli->query($query_sql);
}*/
