<?php
function only(string $type)
{
    if (!isset($_SESSION["type"]) or $_SESSION["type"] != $type) {
        header("location: forbidden.php");
    }
}
?>