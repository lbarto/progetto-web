<?php
global $dbh;

$sellerId = $_SESSION["username"];
$target_dir = "uploads/";

if (isset($_GET["remove"])) {
    $idToRemove = $_GET["remove"];
    $dbh->deleteDream($idToRemove);
}
if (isset($_POST["name"])) {
    $imageName = $_FILES["image"]["name"];
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    move_uploaded_file($_FILES['image']['tmp_name'], $target_file);

    $dbh->addDream(
        $_POST["name"],
        $_POST["description"],
        $_POST["price"],
        $imageName,
        $_POST["amount"],
        $_POST["category"],
        $sellerId
    );
}

$items = $dbh->getProductsFromSeller($sellerId);


$categories = $dbh->getCategories();
?>

<section>
    <h2>Prodotti in vendita</h2>
    <table style="width:100%">
        <tr>
            <th>Nome</th>
            <th>Descrizione</th>
            <th>Prezzo</th>
            <th>Quantità</th>
            <th>Categoria</th>
        </tr>
        <?php foreach ($items as $item) : ?>

            <tr>
                <td><?= $item["nome"] ?></td>
                <td><?= $item["descrizione"] ?></td>
                <td>€ <?= $item["prezzo"] ?></td>
                <td class="quantity"><?= $item["quantita"] ?></td>
                <td class="category"><?= $item["categoria"] ?></td>
                <td>
                    <a class="btn btn-primary" href="?remove=<?= $item["id"] ?>">Elimina</a>
                </td>
            </tr>

        <?php endforeach ?>
    </table>

</section>
<hr>
<section>
    <h2>Aggiungi un prodotto:</h2>
    <form action="?add" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Nome prodotto</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="description">Descrizione prodotto</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <div class="form-group">
            <label for="price">Prezzo prodotto</label>
            <input type="number" class="form-control" id="price" name="price">
        </div>
        <div class="form-group">
            <label for="image">Immagine del prodotto</label>
            <input type="file" class="form-control-file" id="image" name="image">
        </div>
        <div class="form-group">
            <label for="amount">Quantità disponibile</label>
            <input type="number" class="form-control" id="amount" name="amount">
        </div>
        <div class="form-group">
            <label for="category">Categoria</label>
            <select class="form-control" id="category" name="category">
                <?php foreach ($categories as $category) :
                ?>
                    <option value="<?= $category["id"] ?>"><?= $category["nome"] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Aggiungi</button>
    </form>
</section>