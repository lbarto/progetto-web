<?php
require_once "components/ElementCarousel.php";

global $dbh;

$dreams = $dbh->getDreams(10, 0,"");
?>

<div class="alert alert-info mx-3 promotion-alert" id="pickDreamBanner" role="alert">
    <div>Scopri il tuo sogno!</div><a href="wow.php" class="btn btn-primary">Clicca qui!</a>
</div>
<div class="item-grid-heading mx-3">
    <h2>Elementi in evidenza:</h2>
    <a href="shop.php">Sfoglia il catalogo: <i class="fa fa-arrow-right fa-lg mx-2"></i></a>
</div>
<?php ElementCarousel($dreams); ?>