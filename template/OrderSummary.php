<?php
require_once "components/CartList.php";

global $dbh;

if (isset($_SESSION["cartItems"])) {
    $sessionCartItems = $_SESSION["cartItems"];
} else {
    $sessionCartItems = array();
}

$userData = $dbh->getBuyerData($_SESSION["username"]);
$email = $userData["email"];
$card = $userData["carta"];
$censoredCard = substr($card, 0, 4) . "xxxx" . substr($card, 6);

$cartItems = array();
foreach ($sessionCartItems as $itemId => $itemQuantity) {
    $itemData = $dbh->getDream($itemId);
    $itemArray = array("data" => $itemData, "quantity" => $itemQuantity);
    array_push($cartItems, $itemArray);
}

$totalPrice = 0;
$totalQuantity = 0;
foreach ($cartItems as $item) {
    $totalPrice += $item["data"]["prezzo"] * $item["quantity"];
    $totalQuantity += $item["quantity"];
}

if (count($cartItems) > 0) {
?>
    <h1>Riepilogo ordine</h1>
    <section>
        <h2>Elementi</h2>
        <table style="width:100%">
            <tr>
                <th>Prodotto</th>
                <th>Quantità</th>
                <th>Prezzo</th>
            </tr>
            <?php foreach ($cartItems as $item) {
                $data = $item["data"];
            ?>
                <tr>
                    <td><?= $data["nome"] ?></td>
                    <td><?= $item["quantity"] ?></td>
                    <td>€ <?= $data["prezzo"] ?></td>
                </tr>
            <?php } ?>

            <tr>
                <td>Totale:</td>
                <td><?= $totalQuantity ?></td>
                <td>€ <?= $totalPrice ?></td>
            </tr>
        </table>
    </section>
    <hr>
    <section>
        <h2>Informazioni di fatturazione:</h2>
        <p> <strong> Email: </strong><?= $email ?></p>
        <p> <strong> Metodo di fatturazione:</strong> Carta di credito <?= $censoredCard ?></p>
    </section>
    <hr>
    <section>
        <h2>Informazioni di spedizione:</h2>
        Il prodotto ti verrà recapitato direttamente nel sonno, alla prossima dormita.
    </section>
    <hr>
    <a href="ordersuccess.php" class="btn btn-primary">Conferma ordine</a>
<?php
} else { ?>
    Il tuo carrello è vuoto!
<?php
}
?>