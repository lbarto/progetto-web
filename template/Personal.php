<?php
require_once "components/ElementCarousel.php";
require_once "db/database.php";

global $dbh;

if (isset($_GET["logout"])) {
    unset($_SESSION["username"]);
    unset($_SESSION["type"]);
?>
    <div class="jumbotron">
        <h1 class="display-4">LOGOUT AVVENUTO CON SUCCESSO!</h1>
        <p class="lead">Alla prossima, ti aspettiamo!</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="index.php" role="button">Home</a>
        </p>
    </div>
    <?php
} else {
    $db = new DatabaseHelper("localhost", "root", "", "dreamshopper");
    if ($_SESSION["type"] == "Client") {
        $info = $db->getBuyerData($_SESSION['username']);
    ?>

        <table class=" block profileTable">
            <tr>
                <th>Dettagli profilo:</th>
                <th>Azioni:</th>
            </tr>
            <tr>
                <td>
                    <div class="card text-white bg-primary mb-3 profile" style="width: 18rem;">
                        <img class="card-img-top" src="<?php echo $info['foto'] ?>" alt="Card image cap">
                        <div class="card-body">
                            Username: <?php echo $info['username'] ?>
                        </div>
                        <div class="card-body">
                            Email: <?php echo $info['email'] ?>
                        </div>
                        <div class="card-body">
                            Città: <?php echo $info['citta'] ?>
                        </div>
                    </div>
                </td>
                <td class="actionsProfile">
                    <div>
                        <a class="btn btn-primary btn-lg btn-block" href="cart.php">Vai al tuo carrello</a>
                        <a class="btn btn-primary btn-lg btn-block" href="?logout">Fai logout</a>
                    </div>

                </td>

            </tr>
        </table>

    <?php
    } else {
        $info = $db->getSellerData($_SESSION['username']);
    ?>
        <div class="row">
            <div class="col-xl-6">
                Dettagli profilo:
                <div class="card text-white bg-primary mb-3 profile" style="width: 18rem;">
                    <img class="card-img-top" src="<?php echo $info['foto'] ?>" alt="Card image cap">
                    <div class="card-body">
                        Username: <?php echo $info['username'] ?>
                    </div>
                    <div class="card-body">
                        Email: <?php echo $info['email'] ?>
                    </div>
                    <div class="card-body">
                        Città: <?php echo $info['citta'] ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                Azioni:
                <div>
                    <a class="btn btn-primary btn-lg btn-block" href="productmanagement.php">Gestisci i tuoi prodotti in vendita</a>
                    <a class="btn btn-primary btn-lg btn-block" href="?logout">Fai logout</a>
                </div>

            </div>

        </div>


<?php
    }
}
?>