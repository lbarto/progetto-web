<div class="form-container">
    <div class="card form-content">
        <h5 class="card-header">Questionario dei sogni!</h5>
        <div class="card-body">

            <form action="wowresponse.php" method="POST">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Mario">
                </div>
                <div class="form-group">
                    <label for="surname">Cognome</label>
                    <input type="text" class="form-control" name="surname" id="surname" placeholder="Rossi">
                </div>
                <div class="form-group">
                    <label for="age">Età</label>
                    <input type="number" class="form-control" name="age" id="age">
                </div>
                <div class="form-group">
                    <label for="color">Colore preferito</label>
                    <select class="form-control" name="color" id="color">
                        <option>Rosso</option>
                        <option>Verde</option>
                        <option>Giallo</option>
                        <option>Blu</option>
                        <option>Rosa</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Sesso</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sex" id="exampleRadios1" value="f">
                        <label class="form-check-label" for="exampleRadios1">
                            Femmina
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sex" id="exampleRadios2" value="m">
                        <label class="form-check-label" for="exampleRadios2">
                            Maschio
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sex" id="exampleRadios3" value="n">
                        <label class="form-check-label" for="exampleRadios3">
                            Preferisco non rispondere
                        </label>
                    </div>
                </div>
                <input type="submit" value="Invia" class="btn btn-primary" />
            </form>
        </div>

    </div>
</div>