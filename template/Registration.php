<?php
$error = 100;
$login = false;
require_once("./utils/checkDBinfo.php");
require_once("./db/database.php");
require_once("./components/Layout.php");

if (
    isset($_POST['nome']) && isset($_POST['birthday']) && isset($_POST['email'])
    && isset($_POST['password']) && isset($_POST['repeatPassword'])
    && isset($_POST['city']) && isset($_POST['card']) && isset($_POST['userType'])
) {

    $username = $_POST['nome'];
    $type = $_POST['userType'];
    $email = $_POST['email'];
    $pwd = $_POST['password'];
    $db = new DatabaseHelper("localhost", "root", "", "dreamShopper");


    /* controllo che l'email non sia già presente*/
    $resEmail = $db->checkEmailIsPresent($email);
    if ($resEmail) {
        $error = -1;
    }

    /*Controllo che l'username sia unico */
    $resUsername = $db->checkUsernameIsPresent($username);
    if ($resUsername) {
        $error = -5;
    }

    $imgDir = "public/profileImages/";
    $defaultFoto = "public/defaultPhotoProfile.jpg";
    $defaultFormat = "jpg";
    if (!isset($_FILES['profilePhoto']) || !is_uploaded_file($_FILES['profilePhoto']['tmp_name'])) {
        $fotoProfilo = $defaultFoto;
    } else {

        //verifica che sia un immagine
        $is_img = getimagesize($_FILES['profilePhoto']['tmp_name']);
        if (!$is_img) {
            $error = -4;
        } else {

            $userfile_tmp = $_FILES['profilePhoto']['tmp_name'];

            $userfile_name = $_FILES['profilePhoto']['name'];

            $fotoProfilo = $imgDir . basename($userfile_name);

            move_uploaded_file($userfile_tmp, $fotoProfilo);
        }
    }
    if ($error > 0) {
        $res = signIn($type, $username, $_POST['birthday'], $email, $pwd, $_POST['city'], $_POST['card'], $fotoProfilo, $db); // la fotoProfilo va ricercata nella tabella immagini con per id l'email, per questo non passo il parametro
        if (!$res) {
            $error = 1;
        } else {
            $login = true;
        }
    }
}

function errorOccured($format, $msg)
{
    return "<div class='mx-auto " . $format . " alert alert-danger alert-dismissible fade show' id='error' role='alert'>" .
        $msg .
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
        "<span aria-hidden='true'>&times;</span>" .
        "</button>" .
        "</div>";
}

?>

<!-- Bootstrap CSS 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    -->
<!-- CSS
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" type="text/css">
    -->

<!-- Optional JavaScript -->
<!-- jQuery and validator first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js" integrity="sha384-jR1IKAba71QSQwPRf3TY+RAEovSBBqf4Hyp7Txom+SfpO0RCZPgXbINV+5ncw+Ph" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!--<script type="module" src="js/checkRegistration.js"></script>-->

<!-- OR -->

<!-- Check Scripts-->
<script src="js/username.js"></script>

<script src="js/birthday.js"></script>

<script src="js/control_password.js"></script>

<script src="js/check_email.js"></script> <!-- Controllo formato Email-->

<script src="js/city.js"></script>

<script src="js/card.js"></script>

<script src="js/form.js"></script>
<!-- Enrcypted password-->
<script src="js/sha512.js"></script>

<!-- Check email
    <script src="./../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>-->

<!-- Javascript messagges when error -->
<script src="jsUtils/messages_it.min.js"></script>
<!-- TOCHECK-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<?php
if ($login) { ?>
    <div class="jumbotron">
        <h1 class="display-4">REGISTRAZIONE AVVENUTA CON SUCCESSO!</h1>
        <p class="lead">Benvenuto/a <?php echo $_SESSION['username'] ?>!</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="index.php" role="button">Home</a>
        </p>
    </div>
<?php
} else {
?>
    <div class="form-container">
        <div class="card form-container">
            <h1 class="form-header text-center">REGISTRAZIONE</h1>
            <p class="text-center">Entra a far parte di questa famiglia!</p>
            <div class="card-body">
                <?php
                if ($error <= 1) {
                    $format = "col-auto";
                    switch ($error) {
                        case -1:
                            echo errorOccured($format, "L'email è già associata ad un account!");
                            break;
                        case -2:
                            echo errorOccured($format, "Riempire tutti i campi!");
                            break;
                        case -3:
                            echo errorOccured($format, "Connessione errata!");
                            break;
                        case -4:
                            echo errorOccured($format, "La foto per il profilo inserita non è valida, non è un formato riconosciuto!");
                            break;
                        case -5:
                            echo errorOccured($format, "Username già in uso!");
                            break;
                        default:
                            echo errorOccured($format, 'Errore non riconosciuto: Error#');
                            break;
                    }
                    $error = 100;
                }
                ?>
                <form id="input-form" method="POST" onsubmit="return validateForm()" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputNome">USERNAME</label>
                        <input type="text" name="nome" class="form-control" id="inputName" onfocusout="checkName();" placeholder="Inserisci qui il tuo username.">
                        <span id="confirmName" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputNome">DATA DI NASCITA</label>
                        <input type="date" name="birthday" class="form-control" id="inputBirthday" onfocusout="checkBirthday();" required></input>
                        <span id="confirmBirthday" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">EMAIL</label>
                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Inserire email." onfocusout="checkEmail();" required></input>
                        <span id="confirmEmail" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">PASSWORD</label>
                        <input type="password" name="password" class="form-control" id="idPassword" minlength="8" placeholder="Inserire password." onfocusout="controlPassword();" required></input>
                        <span id="confirmPassword" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="repeatPassword">RIPETI PASSWORD</label>
                        <input type="password" name="repeatPassword" class="form-control" id="idRepeatPassword" minlength="5" placeholder="Inserire di nuovo la password." onfocusout="controlPassword();" required></input>
                        <span id="confirmRepeatedPassword" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputCity">CITTA'</label>
                        <input type="text" name="city" class="form-control" id="inputCity" placeholder="Inserire città." onfocusout="checkCity();" required></input>
                        <span id="confirmCity" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputCard">NUMERO CARTA DI CREDITO</label>
                        <input type="text" name="card" class="form-control" id="inputCard" minlength="10" maxlength="10" placeholder="Inserire numero di carta di credito." onfocusout="checkTel(); return false;" required>
                        <span id="confirmTel" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <div class="radioButtonChoice">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="userType" id="radioClient" value="Client" checked>
                                <label class="form-check-label" for="radioClient">
                                    CLIENTE
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="userType" id="radioSeller" value="Seller">
                                <label class="form-check-label" for="radioSeller">
                                    VENDITORE
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputImage">INSERT YOUR PROFILE IMAGE </label>
                        <input type="file" name="profilePhoto" accept="image/*" id="foto" capture></input>
                        <div class="photoSpace">
                            <canvas style="max-width: 310px"></canvas> <!-- TOCHECK -->
                        </div>
                    </div>
                    <div class="registration">
                        <input type="button" value="Home" onClick="location.href='index.php'" name="back home" class="btn btn-primary"></input>
                        <input type="submit" class="btn btn-primary next" id="submit" value="Registrati"></input>
                        <input type="hidden" name="sent" value="true" /></input><!-- schermata di conferma-->
                    </div>
                </form>
            </div>
        </div>
        <!-- <section id="registration window">
            <form id="inputform" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">
                <fieldset id="registration">
                    <div class="headRegistration">
                        <legend>REGISTRAZIONE</legend>
                        <p class="descriptionRegistration">Entra a far parte di questa famiglia!</p>
                    </div>
                    <!--inserire codice php per controllo errore -->
        <!-- <div class="form-group">
                        <label for="inputNome">NOME</label>
                        <input type="text" name="nome" class="form-control" id="inputNome" onfocusout="checkName();" placeholder="Inserisci username" required></input>
                        <span id="confirmName" class="confirmMessage"></span>
                    </div>

                    <div class="form-group">
                        <label for="inputBirthday">DATA DI NASCITA</label>
                        <input type="date" name="birthday" class="form-control" id="inputBirthday" onfocusout="checkBirthday()" required></input>
                        <span id="confirmBirthday" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">EMAIL</label>
                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Inserire email." onfocusout="checkEmail();" required></input>
                        <span id="confirmEmail" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">PASSWORD</label>
                        <input type="password" name="password" class="form-control" id="idPassword" minlength="8" placeholder="Inserire password." onfocusout="controlPassword();" required></input>
                        <span id="confirmPassword" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="repeatPassword">RIPETI PASSWORD</label>
                        <input type="password" name="repeatPassword" class="form-control" id="idRepeatPassword" minlength="5" placeholder="Inserire di nuovo la password." onfocusout="controlPassword();" required></input>
                        <span id="confirmRepeatedPassword" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputCity">CITTA'</label>
                        <input type="text" name="city" class="form-control" id="inputCity" placeholder="Inserire città." onfocusout="checkCity();" required></input>
                        <span id="confirmCity" class="confirmMessage"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputCard">NUMERO CARTA DI CREDITO</label>
                        <input type="text" name="card" class="form-control" id="inputCard" minlength="10" maxlength="10" placeholder="Inserire numero di carta di credito." onfocusout="checkTel(); return false;" required>
                        <span id="confirmTel" class="confirmMessage"></span>
                    </div>
                    <div class="radioButtonChoice">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="userType" id="radioClient" value="Client" checked>
                            <label class="form-check-label" for="radioClient">
                                CLIENTE
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="userType" id="radioSeller" value="Seller">
                            <label class="form-check-label" for="radioSeller">
                                VENDITORE
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputImage">INSERT YOUR PROFILE IMAGE </label>
                        <input type="file" name="profilePhoto" accept="image/*" id="foto" capture></input>
                        <div class="photoSpace">
                            <canvas style="max-width: 310px"></canvas> <!-- TOCHECK -->
        <!-- </div>

                    </div>
    </div>
    </fieldset>
    <div class="registration">
        <input type="button" value="Back home" onClick="location.href='index.php'" name="Back home" class="btn btn-primary"></input>
        <input type="submit" class="btn btn-primary next" id="submit" value="Next"></input>
        <input type="hidden" name="sent" value="true" /></input><!-- schermata di conferma-->
        <!--  </div>
    </form>
    </section>

    </div>-->
    <?php
}
    ?>
    <script data-cfasync="false" src="jsUtils/email-decode.min.js"></script>
    <script>
        function validateForm() {
            let user = checkName();
            if (user == false) {
                alert("Nome errato!");
                return false;
            }
            let birthday = checkBirthday();
            if (birthday == false) {
                alert("Data di nascita errata!");
                return false;
            }
            let email = checkEmail();
            if (email == false) {
                alert("Email errata!");
                return false;
            }
            let city = checkCity();
            if (city == false) {
                alert("Città errata!");
                return false;
            }
            let card = checkCard();
            if (card == false) {
                alert("Numero di carta errato!");
                return false;
            }
            let pass = controlPassword();
            if (pass == false) {
                alert("Una delle due password è errata!");
                return false;
            }
        }
    </script>
    <!-- Check inserimento foto profilo-->
    <script>
        let input = document.querySelector('#foto');

        input.onchange = function() {
            let file = input.files[0];

            upload(file);
            drawOnCanvas(file);
        };

        function upload(file) {
            let form = new FormData(),
                xhr = new XMLHttpRequest();

            form.append('image', file);
        }

        function drawOnCanvas(file) {
            let reader = new FileReader();

            reader.onload = function(e) {
                let dataURL = e.target.result,
                    c = document.querySelector('canvas'),
                    ctx = c.getContext('2d'),
                    img = new Image();

                img.onload = function() {
                    c.width = img.width;
                    c.height = img.height;
                    ctx.drawImage(img, 0, 0);
                };

                img.src = dataURL;
            };

            reader.readAsDataURL(file);
        }
    </script>