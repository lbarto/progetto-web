<?php
require_once("./bootstrap.php");
require_once("./components/Layout.php");
require_once("./db/database.php");

$error = 0;
$login = false;
if (isset($_POST['usernameLogin']) && isset($_POST['passwordLogin']) && isset($_POST['userTypeLogin'])) {
    if ($_POST['usernameLogin']=="" || $_POST['passwordLogin']=="") {
        $error = 1; //non setto le variabili
    } else {
        $dbh = new DatabaseHelper("localhost", "root", "", "dreamshopper");
        $final = $dbh->checkLogin($_POST['userTypeLogin'], $_POST['usernameLogin'], $_POST['passwordLogin']);
        if (!$final) {
            $error = 2; //login non ha avuto successo
            //header("location: ../login.php");
        } else {
            $login = true;
        }
    }
}
//unset($_POST['userTypeLogin'], $_POST['usernameLogin'], $_POST['passwordLogin']); //TOCHECK
?>

<?php
if ($login) { ?>
    <div class="jumbotron">
        <h1 class="display-4">LOGIN AVVENUTO CON SUCCESSO!</h1>
        <p class="lead">Ben tornato/a <?php echo $_SESSION['username'] ?>!</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="index.php" role="button">Home</a>
        </p>
    </div>
<?php
} else {
?>
    <section>
        <form method="POST">
            <legend class="w-50 text-center">LOGIN</legend>

            <?php if (isset($error)) {
                switch ($error) {
                    case 1:
                        echo '<div class="alert alert-warning mx-3" role="alert">
                                Completa tutti i campi!
                            </div>';
                        break;
                    case 2:
                        echo '<div class="alert alert-warning mx-3" role="alert">
                                Username o password errati!
                            </div>';
                        break;
                    case 0:
                        echo "";
                        break;
                    default:
                        echo '<div class="alert alert-warning mx-3" role="alert">
                                Errore sconosciuto!
                            </div>';
                }
            } ?>
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" name="usernameLogin" id="usernameLogin" aria-describedby="usernameHelp">
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" name="passwordLogin" id="passwordLogin">
            </div>
            <div class="radioButtonChoice">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="userTypeLogin" id="radioClient" value="Client" checked>
                    <label class="form-check-label" for="radioClient">
                        CLIENTE
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="userTypeLogin" id="radioSeller" value="Seller">
                    <label class="form-check-label" for="radioSeller">
                        VENDITORE
                    </label>
                </div>
            </div>
            <button id="login" type="submit" class="btn btn-primary">LOGIN</button>
        </form>
    </section>
    <hr>
    <section class="inviteRegistration">
        Non sei ancora iscritto?
        <a class="btn btn-primary" href="registration.php">Registrati adesso!</a>
    </section>
<?php
}
?>