<?php

require_once "components/CartList.php";

global $dbh;

if (isset($_SESSION["cartItems"])) {

    if (isset($_GET["remove"])) {
        unset($_SESSION["cartItems"][$_GET["remove"]]);
    }

    $sessionCartItems = $_SESSION["cartItems"];
} else {
    $sessionCartItems = array();
}

$cartItems = array();
foreach ($sessionCartItems as $itemId => $itemQuantity) {
    $itemData = $dbh->getDream($itemId);
    $itemArray = array("data" => $itemData, "quantity" => $itemQuantity);
    array_push($cartItems, $itemArray);
}

$totalPrice = 0;
$totalQuantity = 0;
foreach ($cartItems as $item) {
    $totalPrice += $item["data"]["prezzo"] * $item["quantity"];
    $totalQuantity += $item["quantity"];
}

$totalPrice = 0;
$totalQuantity = 0;
foreach ($cartItems as $item) {
    $totalPrice += $item["data"]["prezzo"] * $item["quantity"];
    $totalQuantity += $item["quantity"];
}

?>
<table style="width:100%">
    <tr>
        <th>Prodotto</th>
        <th>Quantità</th>
        <th>Prezzo</th>
        <th>Azioni</th>
    </tr>
    <?php foreach ($cartItems as $item) {
        $data = $item["data"];
    ?>
        <tr>
            <td><?= $data["nome"] ?></td>
            <td><?= $item["quantity"] ?></td>
            <td>€ <?= $data["prezzo"] ?></td>
            <td><a href="?remove=<?= $data["id"] ?>">Elimina articolo</a></td>
        </tr>
    <?php } ?>

    <tr>
        <td>Totale:</td>
        <td><?= $totalQuantity ?></td>
        <td>€ <?= $totalPrice ?></td>
    </tr>
</table>
<a href="order.php" class="btn btn-primary">Procedi all'acquisto</a>