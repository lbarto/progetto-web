<?php
require_once "components/ElementCarousel.php";

global $dbh;

//get page amount
$pageSize = 2;

if (isset($_GET["category"])) {
    $category = $_GET["category"];
    $count = $dbh->getDreamsCountFromCategory($category);
} else {
    $count = $dbh->getDreamsCount();
}

$pageCount = $count / $pageSize;

if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 0;
}

$pageOffset = $page * $pageSize;


//get order
if (isset($_GET["orderby"])) {
    $orderBy = $_GET["orderby"];
} else {
    $orderBy = "az";
}


//get items based on category
if (isset($_GET["category"])) {
    $category = $_GET["category"];
    $dreams = $dbh->getDreamsFromCategory($pageSize, $pageOffset, $category, $orderBy);
} else {
    $dreams = $dbh->getDreams($pageSize, $pageOffset, $orderBy);
}


//prepare category url parameter
if (isset($_GET["category"])) {
    $categoryUrl = "&category=" . $_GET["category"];
} else {
    $categoryUrl = "";
}

//prepare ordering url parameter
if (isset($_GET["orderby"])) {
    $orderByUrl = "&orderby=" . $_GET["orderby"];
} else {
    $orderByUrl = "";
}


?>


<div class="item-grid-heading mx-3">
    <h2>I nostri articoli:</h2>
</div>
<?php ElementCarousel($dreams); ?>
<nav aria-label="Page navigation example">
    <ul class="pagination">
        <?php if ($page > 0) : ?>
            <li class="page-item">
                <a class="page-link" href="?page=<?= ($page - 1) . $categoryUrl ?>">Precedente</a>
            </li>
        <?php endif ?>
        <?php for ($i = 0; $i < $pageCount; $i++) : ?>
            <li class="page-item <?= $i == $page ? "active" : "" ?>">
                <a class="page-link" href="?page=<?= $i . $categoryUrl ?>">
                    <?= $i + 1 ?>
                </a>
            </li>
        <?php endfor ?>
        <?php if ($page < $pageCount - 1) : ?>
            <li class="page-item"><a class="page-link" href="?page=<?= ($page + 1) . $categoryUrl ?>">Successiva</a></li>
        <?php endif ?>
    </ul>
</nav>