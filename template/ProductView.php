<?php
require_once "components/ElementCarousel.php";

global $dbh;
global $isSeller, $isClient;


//aggiunge l'elemento al carrello
if (!$isSeller) {
    if (isset($_GET["id"]) && isset($_POST["quantity"])) {
        $id = $_GET["id"];
        $quantity = intval($_POST["quantity"]);

        if ($quantity <= 0) {

?>
            <div class="alert alert-danger mx-3 promotion-alert" id="pickDreamBanner" role="alert">
                ELEMENTO NON AGGIUNTO AL CARRELLO!
            </div>

<?php

        } else {

            if (!isset($_SESSION["cartItems"])) {
                $_SESSION["cartItems"] = array();
            }

            if (!isset($_SESSION["cartItems"][$id])) {
                $_SESSION["cartItems"][$id] = $quantity;
            } else {
                $_SESSION["cartItems"][$id] += $quantity;
            }
        }
    }
}

//prepare category url parameter
if (isset($_GET["id"])) {
    $productId = $_GET["id"];
} else {
    $productId = "sogno3";
}

$dream = $dbh->getDream($productId);
$moreDreams = $dbh->getDreamsFromCategory(4, 0, $dream["categoria"], "");
$currentUrl = "https://www.dream-shopper.it/product.php?id=" . $dream["id"];
?>

<script>
    function checkQuantity() {
        let quantity = $('#quantity').val();
        if (quantity <= 0) {
            document.getElementById("negativeQuantity").innerHTML = "La quantità deve essere maggiore di zero!";
            document.getElementById("negativeQuantity").style.color = "#ff6666";
        } else {
            document.getElementById("negativeQuantity").innerHTML = "";
        }
    }
</script>


<div class="product-desc">

    <div class="row mb-3">
        <div class="col-4 mb-5">
            <img class="main-image px-4" src="uploads/<?= $dream["immagine"] ?>" alt="">
        </div>
        <div class="col-xl-5 mb-5">
            <h1 class="display-4"><?= $dream["nome"] ?></h1>
            <p>Venduto da <?= $dream["nomeVenditore"] ?>
                <hr />
            <p class="my-4"><span class="price">€ <?= $dream["prezzo"] ?> </span><span class="text-muted">iva inclusa</span></p>
            <h2>Informazioni su questo sogno:</h2>
            <p>
                <?= $dream["descrizione"] ?>
            </p>
            <div class="share-section">

                <h2>Condividi: </h2>
                <a href="whatsapp://send?text=<?= $currentUrl ?>">
                    <i class="fa fa-whatsapp fa-2x"></i>
                </a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $currentUrl ?>">
                    <i class="fa fa-facebook-square fa-2x"></i>
                </a>
                <a href="https://twitter.com/intent/tweet?text=<?= $currentUrl ?>">
                    <i class="fa fa-twitter-square fa-2x"></i>
                </a>
                <a href="https://t.me/share/url?url=<?= $currentUrl ?>">
                    <i class="fa fa-telegram fa-2x"></i>
                </a>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card add-to-cart-card">
                <div class="card-body">
                    <p class="text-success">DISPONIBILE</p>
                    <p>Solo <?= $dream["quantita"] ?> in magazzino</p>
                    <?php if (!$isSeller) : ?>
                        <form method="POST">
                            <input type="number" name="quantity" id="quantity" onfocusout="checkQuantity()">
                            <div id="negativeQuantity"></div>
                            <button class="btn"><i class="mr-2 fa fa-shopping-cart fa-lg"></i>Aggiungi al carrello!</button>
                        </form>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row">
    <div class="item-grid-heading mx-3">
        <h2>Altri articoli simili:</h2>
    </div>
    <?php ElementCarousel($moreDreams) ?>
</div>