<div class="jumbotron">
    <h1 class="display-4">Area riservata</h1>
    <p class="lead">Questa area è riservata agli utenti registrati. Fai login per accedere.</p>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="login.php" role="button">Login</a>
    </p>
</div>