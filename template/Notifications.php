<?php
global $dbh;
$sellerId = $_SESSION["username"];

if (isset($_GET["clear"])) {
    $dbh->clearNotification($_GET["clear"], $sellerId);
}

$currentNotifications = $dbh->getUnresolvedNotifications($sellerId);
$pastNotifications =  $dbh->getResolvedNotifications($sellerId);
?>
<section>
    <h2>Nuove notifiche</h2>
    <table style="width:100%">
        <tr>
            <th>Data</th>
            <th>Messaggio</th>
            <th>Azioni</th>
        </tr>

        <?php foreach ($currentNotifications as $not) : ?>
            <tr>
                <td><?= $not["data"] ?></td>
                <td><?= $not["contenuto"] ?></td>
                <td><a href="?clear=<?= $not["id"] ?>">Segna come letto</a></td>
            </tr>

        <?php endforeach ?>

    </table>
</section>
<hr>
<section>
    <h2>Notifiche passate</h2>
    <table style="width:100%">
        <tr>
            <th>Data</th>
            <th>Messaggio</th>
            <th>Azioni</th>
        </tr>

        <?php foreach ($pastNotifications as $not) : ?>
            <tr>
                <td><?= $not["data"] ?></td>
                <td><?= $not["contenuto"] ?></td>
                <td></td>
            </tr>

        <?php endforeach ?>

    </table>
</section>