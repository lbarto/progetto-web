<?php
global $dbh;

if (isset($_GET["category"])) {
    $currentCategory = $_GET["category"];
} else {
    $currentCategory = -1;
}

$categories = $dbh->getCategories();
?>
<div class="list-group list-group-flush">

    <div class="list-group-item list-group-item-dark">Categorie</div>
    <a href="?" type="button" class="list-group-item list-group-item-action <?= -1 == $currentCategory ? "active" : "" ?>">Tutto</a>

    <?php foreach ($categories as $item) : ?>
        <a href="?category=<?= $item["id"] ?>" type="button" class="list-group-item list-group-item-action <?= $item["id"] == $currentCategory ? "active" : "" ?>"><?= $item["nome"] ?></a>
    <?php endforeach ?>
</div>

<?php
//get order
if (isset($_GET["orderby"])) {
    $orderBy = $_GET["orderby"];
} else {
    $orderBy = "az";
}

//prepare category url parameter
if (isset($_GET["category"])) {
    $categoryUrl = "&category=" . $_GET["category"];
} else {
    $categoryUrl = "";
}

$orderModes = [
    array("id" => "az", "display" => "Alfabetico (a-z)"),
    array("id" => "za", "display" => "Alfabetico (z-a)"),
    array("id" => "price", "display" => "Prezzo (crescente)"),
    array("id" => "priceInv", "display" => "Prezzo (decrescente)")
]
?>
<div class="list-group list-group-flush">

    <div class="list-group-item list-group-item-dark">Categorie</div>

    <?php foreach ($orderModes as $item) : ?>
        <a href="?orderby=<?= $item["id"] . $categoryUrl ?>" type="button" class="list-group-item list-group-item-action <?= $item["id"] == $orderBy ? "active" : "" ?>">
            <?= $item["display"] ?>
        </a>
    <?php endforeach ?>
</div>