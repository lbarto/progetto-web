<?php

global $dbh;

if (isset($_SESSION["cartItems"])) {
    $sessionCartItems = $_SESSION["cartItems"];


    $cartItems = array();
    foreach ($sessionCartItems as $itemId => $itemQuantity) {
        $itemData = $dbh->getDream($itemId);
        $itemArray = array("data" => $itemData, "quantity" => $itemQuantity);
        array_push($cartItems, $itemArray);
    }

    foreach ($cartItems as $item) {
        $amount = $item["quantity"];
        $id = $item["data"]["id"];
        $dbh->decrementDream($id, $amount);
        $dbh->createNotification(
            $item["data"]["venditore"],
            "L'utente " . $_SESSION["username"] . " ha appena acquistato " . $item["quantity"] . " unità del tuo sogno " . $item["data"]["nome"]
        );
    }

    unset($_SESSION["cartItems"]);
?>

    <div class="jumbotron">
        <h1 class="display-4">Evviva!</h1>
        <p class="lead">Ordine andato a buon fine</p>
        </p>
    </div>

<?php
} else {
?>

    <div class="jumbotron">
        <h1 class="display-4">Errore</h1>
        <p class="lead">Il tuo carrello è vuoto.</p>
        </p>
    </div>
<?php
}
?>