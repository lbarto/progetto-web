<?php
function Layout(
    $title,
    $description,
    $mainComponent,
    $asideComponent
) {
    global $isSeller, $isClient;
?>
    <!DOCTYPE html>
    <html lang="it">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= "Dream Shopper - " . $title ?>
        </title>
        <meta name="description" content="<?= $description ?>">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/shopper-style.css">
    </head>

    <body>
        <nav class="shadow">
            <div class="navbar navbar-expand navbar-light bg-light">
                <a class="navbar-brand" href="index.php">Dream Shopper</a>
                <div class="ml-auto">
                    <ul class="navbar-nav">
                        <?php if (!$isSeller) : ?>
                            <li class="nav-item">
                                <a class="nav-link" href="cart.php"><i class="fa fa-shopping-cart fa-lg"></i></i></a>
                            </li>
                        <?php endif ?>
                        <li class="nav-item">
                            <a class="nav-link" href="personal.php"><i class="fa fa-user fa-lg"></i></a>
                        </li>
                        <?php if ($isSeller) : ?>
                            <li class="nav-item">
                                <a class="nav-link" href="notifications.php"><i class="fa fa-envelope fa-lg"></i></a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
            <div class="navbar navbar-expand navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-bars fa-lg"></i>
                </button>

                <ul class="navbar-nav d-none d-xl-flex">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="shop.php">Articoli</a>
                    </li>
                </ul>


            </div>
            <div class="collapse navbar navbar-light bg-light" id="collapseExample">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="shop.php">Articoli</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid main-content d-flex">
            <div class="row content-row">
                <main class="col-xl-10 px-5 my-5">
                    <?php require($mainComponent) ?>
                </main>

                <aside class="col-xl-2 order-xl-first my-5">
                    <?php require($asideComponent) ?>
                </aside>
            </div>
        </div>

        <footer class="text-center actual-footer bg-primary text-white py-3">
            <p>Copyright Pablita Industries 2021-2022. All rights reserved</p>
            <p><a id="contacts" href="mailto:spam.farlocco@gmail.com">Contattaci</a></p>
        </footer>

    </body>

    </html>

<?php
}
?>