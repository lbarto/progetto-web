<?php

function CartList($cartItems)
{
    $totalPrice = 0;
    $totalQuantity = 0;
    foreach ($cartItems as $item) {
        $totalPrice += $item["data"]["prezzo"] * $item["quantity"];
        $totalQuantity += $item["quantity"];
    }
?>
    <table style="width:100%">
        <tr>
            <th>Prodotto</th>
            <th>Quantità</th>
            <th>Prezzo</th>
        </tr>
        <?php foreach ($cartItems as $item) {
            $data = $item["data"];
        ?>
            <tr>
                <td><?= $data["nome"] ?></td>
                <td><?= $item["quantity"] ?></td>
                <td>€ <?= $data["prezzo"] ?></td>
            </tr>
        <?php } ?>

        <tr>
            <td>Totale:</td>
            <td><?= $totalQuantity ?></td>
            <td>€ <?= $totalPrice ?></td>
        </tr>
    </table>
<?php
}
?>