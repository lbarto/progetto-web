<?php

function ElementCarousel($items)
{ ?>
    <div class="item-grid">

        <?php foreach ($items as $item) : ?>
            <a class="card-link" href="product.php?id=<?= $item["id"] ?>">
                <div class="card m-3 item-card">
                    <div class="card-img-top carousel-image" style="background-image: url('uploads/<?= $item["immagine"] ?>')"></div>
                    <div class="card-body">
                        <h5 class="card-title"><?= $item["nome"] ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted"><?= "€ " . $item["prezzo"] ?></h6>
                        <p class="card-text"><?= $item["descrizione"] ?></p>
                    </div>
                </div>
            </a>
        <?php endforeach ?>
        <?php for ($i = 0; $i < 4; $i++) : ?>

            <div class="hack-item m-3"></div>
        <?php endfor ?>
    </div>
<?php
}
?>