<?php
require_once "bootstrap.php";
require_once "components/Layout.php";
require_once "utils/ForbiddenTools.php";

only("Client");

Layout(
    "Login",
    "La pagina di login del sito di e-commerce dei tuoi sogni",
    "template/OrderSummary.php",
    "template/SideAd.php"
);