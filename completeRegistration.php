<?php
require_once "bootstrap.php";
require_once "components/Layout.php";

Layout(
    "Registrazione",
    "La pagina di registrazione del sito di e-commerce dei tuoi sogni",
    "template/checkRegistrationDB.php",
    "template/SideAd.php"
);