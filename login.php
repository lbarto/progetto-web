<?php
require_once "bootstrap.php";
require_once "components/Layout.php";

Layout(
    "Login",
    "La pagina di login del sito di e-commerce dei tuoi sogni",
    "template/loginUI.php",
    "template/SideAd.php"
);