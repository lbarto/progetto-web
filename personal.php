<?php
require_once "bootstrap.php";
require_once "components/Layout.php";

if (!isset($_SESSION["type"])) {
    header("location: forbidden.php");
}

Layout(
    "Login",
    "La pagina di login del sito di e-commerce dei tuoi sogni",
    "template/Personal.php",
    "template/SideAd.php"
);
