<?php
require_once "bootstrap.php";
require_once "components/Layout.php";
require_once "utils/ForbiddenTools.php";

only("Seller");

Layout(
    "Login",
    "La pagina di login del sito di e-commerce dei tuoi sogni",
    "template/Notifications.php",
    "template/SideAd.php"
);