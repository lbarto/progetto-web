<?php
require_once "bootstrap.php";
require_once "components/Layout.php";

Layout(
    "Home",
    "La Home page del sito di e-commerce dei tuoi sogni",
    "template/Catalogue.php",
    "template/Categories.php"
);
