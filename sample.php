<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dream Shopper</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/shopper-style.css">
</head>

<body>
    <nav class="shadow">
        <div class="navbar navbar-expand navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <div class="ml-auto">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-shopping-cart fa-lg"></i></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-user fa-lg"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-envelope fa-lg"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar navbar-expand navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <i class="fa fa-bars fa-lg"></i>
            </button>

            <ul class="navbar-nav d-none d-xl-flex">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cuore</a>
                </li>
            </ul>
            <form class="input-group mx-auto">
                <input type="text" class="form-control " placeholder="Enter Name Here">
                <button class="btn">
                    <i class="fa fa-search fa-lg"></i>
                </button>
            </form>

        </div>
        <div class="collapse navbar navbar-light bg-light" id="collapseExample">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cuore</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <section class="col-xl-10 p-5">
                <div class="alert alert-danger mx-3 promotion-alert" role="alert">
                    <div>Scopri il tuo sogno!</div><button type="button" class="btn btn-primary">Clicca qui!</button>
                </div>
                <div class="item-grid-heading mx-3">
                    <h2>Elementi in evidenza:</h2>
                    <button>Sfoglia il catalogo: <i class="fa fa-arrow-right fa-lg mx-2"></i></button>
                </div>
                <div class="item-grid">
                    <?php for ($i = 0; $i < 6; $i++) : ?>
                        <div class="card m-3 item-card">
                            <img class="card-img-top" src="public/trita.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    <?php endfor; ?>
                    <?php for ($i = 0; $i < 4; $i++) : ?>
                        <div class="hack-item m-3"></div>
                    <?php endfor; ?>
                </div>
            </section>

            <section class="col-xl-2 order-xl-first ad"></section>
        </div>
    </div>
    <div class="footer">
        <footer class="text-center actual-footer py-3">
            <p>Copyright Pablita Industries 2021-2022. All rights reserved</p>
            <p><a href="mailto:spam.farlocco@gmail.com">Contattaci</a></p>
        </footer>
    </div>
</body>

</html>