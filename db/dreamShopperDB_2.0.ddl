-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Sun Jan 24 06:24:21 2021 
-- * LUN file: C:\xampp\htdocs\progetto-web\db\dreamShopper.lun 
-- * Schema: logico/2.0 
-- ********************************************* 


-- Database Section
-- ________________ 

create database dreamShopper;
use dreamShopper;


-- Tables Section
-- _____________ 

create table ACQUISTO (
     id bigint not null,
     quantità bigint not null,
     data date not null,
     cliente varchar(100) not null,
     sogno varchar(100) not null,
     constraint IDACQUISTO primary key (id));

create table CATEGORIA (
     id varchar(100) not null,
     nome varchar(1000) not null,
     descrizione varchar(1000) not null,
     constraint IDCATEGORIA primary key (id));

create table CLIENTE (
     username varchar(100) not null,
     password varchar(1000) not null,
     compleanno date not null,
     email varchar(1000) not null,
     citta varchar(1000) not null,
     carta bigint not null,
     foto varchar(1000) not null,
     constraint IDPERSONA primary key (username));

create table SOGNO (
     id varchar(100) not null,
     nome varchar(1000) not null,
     descrizione varchar(3000) not null,
     prezzo bigint not null,
     immagine varchar(1000) not null,
     quantita bigint not null,
     categoria varchar(100) not null,
     venditore varchar(100) not null,
     constraint IDSOGNO primary key (id));

create table VENDITORE (
     username varchar(100) not null,
     password varchar(1000) not null,
     compleanno date not null,
     email varchar(1000) not null,
     citta varchar(1000) not null,
     carta bigint not null,
     foto varchar(1000) not null,
     constraint IDPERSONA primary key (username));


-- Constraints Section
-- ___________________ 

alter table ACQUISTO add constraint FK_CLIENTE
     foreign key (cliente)
     references CLIENTE (username);

alter table ACQUISTO add constraint FK_ACQUISTO
     foreign key (sogno)
     references SOGNO (id);

alter table SOGNO add constraint FK_CATEGORIA
     foreign key (categoria)
     references CATEGORIA (id);

alter table SOGNO add constraint FK_SOGNO
     foreign key (venditore)
     references VENDITORE (username);


-- Index Section
-- _____________ 

