<?php

function getOrderBy(string $order)
{
    switch ($order) {
        case "za":
            return "nome DESC";
        case "price":
            return "prezzo ASC";
        case "priceInv":
            return "prezzo DESC";
        default:
            return "nome ASC";
    }
}

class DatabaseHelper
{
    private $db;

    public function __construct($servername, $username, $password, $dbname)
    {
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("DB connection failed");
        }
    }

    public function getDreams($limit, $offset, $order)
    {
        $orderBy = getOrderBy($order);

        $stmt = $this->db->prepare(
            "SELECT *
            FROM sogno
            ORDER BY " . $orderBy . "
            LIMIT ?
            OFFSET ?"
        );
        $stmt->bind_param("ii", $limit, $offset);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDream($id)
    {
        $stmt = $this->db->prepare(
            "SELECT s.*, v.username as nomeVenditore
            FROM sogno as s, venditore as v
            WHERE s.venditore=v.username
            AND s.id=?"
        );
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }
    public function getDreamsCount()
    {
        $stmt = $this->db->prepare(
            "SELECT COUNT(*)
            FROM sogno"
        );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0]["COUNT(*)"];
    }

    public function getDreamsFromCategory($limit, $offset, $category, $order)
    {
        $orderBy = getOrderBy($order);
        $stmt = $this->db->prepare(
            "SELECT *
            FROM sogno
            WHERE categoria=?
            ORDER BY " . $orderBy . "
            LIMIT ?
            OFFSET ?"
        );
        $stmt->bind_param("sii", $category, $limit, $offset);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDreamsCountFromCategory($category)
    {
        $stmt = $this->db->prepare(
            "SELECT COUNT(*)
            FROM sogno
            WHERE categoria=?"
        );
        $stmt->bind_param("s", $category);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0]["COUNT(*)"];
    }

    public function getCategories()
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM categoria"
        );
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function decrementDream($dreamId, $amount)
    {
        $stmt = $this->db->prepare(
            "UPDATE sogno
            SET quantita=quantita-?
            WHERE id=?"
        );
        $stmt->bind_param("is", $amount, $dreamId);
        $stmt->execute();

        $stmt = $this->db->prepare(
            "DELETE FROM sogno
            WHERE id=?
            AND quantita<1"
        );
        $stmt->bind_param("s", $dreamId);
        $stmt->execute();
    }

    public function addBuyRecord($amount, $clientId, $dreamId)
    {
        $stmt = $this->db->prepare(
            "INSERT INTO acquisto (id,quantità,data,cliente,sogno)
            VALUES (7,?,CURDATE(),?,?)"
        );
        $stmt->bind_param("iss", $amount, $clientId, $dreamId);
        $stmt->execute();
    }

    public function getProductsFromSeller($sellerId)
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM sogno
            WHERE venditore=?"
        );
        $stmt->bind_param("s", $sellerId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteDream($id)
    {
        $stmt = $this->db->prepare(
            "DELETE FROM sogno
            WHERE id=?"
        );
        $stmt->bind_param("s", $id);
        $stmt->execute();
    }

    public function addDream($name, $desc, $price, $image, $amount, $cat, $seller)
    {
        $stmt = $this->db->prepare(
            "INSERT INTO sogno
            (`nome`, `descrizione`, `prezzo`, `immagine`, `quantita`, `categoria`, `venditore`)
            VALUES (?,?,?,?,?,?,?)"
        );
        $stmt->bind_param("ssisiss", $name, $desc, $price, $image, $amount, $cat, $seller);
        $stmt->execute();
    }

    public function checkLogin($type, $username, $password)
    {
        $final = false;
        if ($type == "Client") {
            $query = $this->db->prepare(
                "SELECT username, password, salt
                FROM cliente
                WHERE username=?"
            );
            $query->bind_param("s", $username);
            $query->execute();
            $res = $query->get_result();
            if (mysqli_num_rows($res) > 0) {
                $result = $res->fetch_all(MYSQLI_ASSOC);
                $checkPass = hash('sha512', $password . $result[0]['salt']);
                $final = $result[0]['password'] == $checkPass;

                //return $final;
            }
        } elseif ($type == "Seller") {
            $query = $this->db->prepare(
                "SELECT username, password, salt
                FROM venditore
                WHERE username=?"
            );

            $query->bind_param("s", $username);
            $query->execute();
            $res = $query->get_result();

            if (mysqli_num_rows($res) > 0) {

                $result = $res->fetch_all(MYSQLI_ASSOC);

                $checkPass = hash('sha512', $password . $result[0]['salt']);
                $final = $result[0]['password'] == $checkPass;

                //return $final;
            }
        }

        if ($final) {
            $_SESSION['username'] = $username;
            $_SESSION['type'] = $type; //type può essere Client o Seller 
        }
        return $final;
    }

    function checkUsernameIsPresent($username)
    {

        $utenti = array(
            "venditore" => "username",
            "cliente" => "username"
        );
        foreach ($utenti as $key => $value) { // check for admin, cliente, fornitore
            /*$stmt = $this->db->prepare("SELECT ? FROM ? WHERE ?=?");
            if ($stmt === FALSE) {
                echo "Mysql Error: ".$this->db->error."";
           }*/

            if ($res = $this->db->prepare("SELECT $value FROM $key WHERE $value =?")) {
                $res->bind_param('s', $username); // bind with '$email'.
                $res->execute();
                $result = $res->get_result();
                $result->fetch_all(MYSQLI_ASSOC);
                if (mysqli_num_rows($result) > 0) {
                    return true;
                }
            }
        }
        return false;
    }
    function checkEmailIsPresent($email)
    {

        $utenti = array(
            "venditore" => "email",
            "cliente" => "email",
        );
        foreach ($utenti as $key => $value) {
            if ($res = ($this->db->prepare("SELECT $value FROM $key WHERE $value=?"))) {
                $res->bind_param('s', $email);  // bind with '$email'.
                $res->execute();
                $final = $res->get_result();
                $final->fetch_all(MYSQLI_ASSOC);
                if (mysqli_num_rows($final) > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    function getDB()
    {
        return $this->db;
    }

    public function getUnresolvedNotifications($id)
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM notifica
            WHERE per=?
            AND visto=false"
        );
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getResolvedNotifications($id)
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM notifica
            WHERE per=?
            AND visto=true"
        );
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function clearNotification($notificationId, $sellerId)
    {

        $stmt = $this->db->prepare(
            "UPDATE notifica
            SET visto=true
            WHERE id=?
            AND per=?"
        );
        $stmt->bind_param("is", $notificationId, $sellerId);
        $stmt->execute();
    }
    public function createNotification($sellerId, $content)
    {

        $stmt = $this->db->prepare(
            "INSERT INTO notifica
            (`per`, `contenuto`, `visto`, `data`)
            VALUES
            (?,?,false,CURRENT_DATE)"
        );
        $stmt->bind_param("ss", $sellerId, $content);
        $stmt->execute();
    }

    public function getBuyerData($username)
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM cliente
            WHERE username=?"
        );
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }

    public function getSellerData($username)
    {
        $stmt = $this->db->prepare(
            "SELECT *
            FROM venditore
            WHERE username=?"
        );
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }
}
