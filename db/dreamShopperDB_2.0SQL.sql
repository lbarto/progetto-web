-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dreamshopper
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acquisto`
--

DROP TABLE IF EXISTS `acquisto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acquisto` (
  `id` bigint NOT NULL,
  `quantità` bigint NOT NULL,
  `data` date NOT NULL,
  `cliente` varchar(100) NOT NULL,
  `sogno` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categoria` (
  `id` varchar(100) NOT NULL,
  `nome` varchar(1000) NOT NULL,
  `descrizione` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `username` varchar(100) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `compleanno` date NOT NULL,
  `email` varchar(1000) NOT NULL,
  `citta` varchar(1000) NOT NULL,
  `carta` bigint NOT NULL,
  `foto` varchar(1000) NOT NULL,
  `salt` varchar(1000) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sogno`
--

DROP TABLE IF EXISTS `sogno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sogno` (
  `id` varchar(100) NOT NULL,
  `nome` varchar(1000) NOT NULL,
  `descrizione` varchar(3000) NOT NULL,
  `prezzo` bigint NOT NULL,
  `immagine` varchar(1000) NOT NULL,
  `quantita` bigint NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `venditore` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venditore`
--

DROP TABLE IF EXISTS `venditore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `venditore` (
  `username` varchar(100) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `compleanno` date NOT NULL,
  `email` varchar(1000) NOT NULL,
  `citta` varchar(1000) NOT NULL,
  `carta` bigint NOT NULL,
  `foto` varchar(1000) NOT NULL,
  `salt` varchar(1000) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `acquisto` 
ADD FOREIGN KEY (`cliente`)
REFERENCES `cliente`(`username`);
    
ALTER TABLE `acquisto` 
ADD FOREIGN KEY (`sogno`)
REFERENCES `sogno`(`id`);
     
ALTER TABLE `sogno` 
ADD FOREIGN KEY (`categoria`)
REFERENCES `categoria`(`id`);
     
ALTER TABLE `sogno` 
ADD FOREIGN KEY (`venditore`)
REFERENCES `venditore`(`username`);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'dreamshopper'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-24  6:52:04
