/*prima creare database*/

CREATE DATABASE dreamShopper


/*poi aggiungere tutte le tabelle e il resto*/

create table `ACQUISTO` (
     `id` varchar(1000) not null,
     `quantità` numeric(10) not null,
     `data` date not null,
     `cliente` varchar(1000) not null,
     `sogno` varchar(1000) not null
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table CATEGORIA (
     `id` varchar(1000) not null,
     `nome` varchar(1000) not null,
     `descrizione` varchar(3000) not null
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table CLIENTE (
     `id` varchar(1000) not null,
     `nome` varchar(1000) not null,
     `compleanno` date not null,
     `username` varchar(1000) not null,
     `password` varchar(1000) not null,
     `numero_carta` varchar(1000) not null,
     `salt` varchar(1000) not null
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table SOGNO (
     `id` varchar(1000) not null,
     `nome` varchar(1000) not null,
     `descrizione` varchar(3000) not null,
     `prezzo` numeric(10) not null,
     `immagine` varchar(3000) not null,
     `quantita` numeric(10) not null,
     `categoria` varchar(1000) not null,
     `venditore` varchar(1000) not null
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table VENDITORE (
     `id` varchar(1000) not null,
     `nome` varchar(1000) not null,
     `compleanno` date not null,
     `username` varchar(1000) not null,
     `password` varchar(1000) not null,
     `numero_carta` varchar(1000) not null,
     `salt` varchar(1000) not null
)ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `ACQUISTO`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `CATEGORIA`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `CLIENTE`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `SOGNO`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `VENDITORE`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ACQUISTO` 
ADD FOREIGN KEY (`cliente`)
REFERENCES `CLIENTE`(`id`);
    
ALTER TABLE `ACQUISTO` 
ADD FOREIGN KEY (`sogno`)
REFERENCES `SOGNO`(`id`);
     
ALTER TABLE `SOGNO` 
ADD FOREIGN KEY (`categoria`)
REFERENCES `CATEGORIA`(`id`);
     
ALTER TABLE `SOGNO` 
ADD FOREIGN KEY (`venditore`)
REFERENCES `VENDITORE`(`id`);

