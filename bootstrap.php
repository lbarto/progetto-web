<?php
session_start();
require_once("db/database.php");
$dbh = new DatabaseHelper("localhost", "root", "", "dreamshopper");
$isSeller = false;
$isClient = false;
if (isset($_SESSION["type"])) {
    if ($_SESSION["type"] == "Client") {
        $isClient = true;
    }
    if ($_SESSION["type"] == "Seller") {
        $isSeller = true;
    }
}
